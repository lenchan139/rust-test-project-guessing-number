#![allow(non_snake_case)]
extern crate rand;

use rand::Rng;
use std::io;
use std::cmp::Ordering;

fn main() {
    guessing_number();

}
fn guessing_number() -> bool{

    loop{

        let random_number = rand::thread_rng().gen_range(1, 101);
            println!("Guess a number.");
            println!("enter your guess number");
            let mut guessStr = String::new();
            io::stdin().read_line(&mut guessStr).expect("Failed to read this line");
            let guess: u32 = match guessStr.trim().parse() {
                Ok(num) => num,
                Err(_) => continue
            };
            println!("You guessed: {}", guess);
            println!("the random number is {}", random_number);
            match guess.cmp(&random_number) {
               Ordering::Less    => println!("Too small!"),
               Ordering::Greater => println!("Too big!"),
               Ordering::Equal   => {
                   println!("You win!");

           },
           }
          }


}
